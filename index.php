<!DOCTYPE html>

<html lang="en">
<head>
  <meta charset="utf-8">

  <title> Archive- archive.ayadanconlangs.com </title>
  <meta name="description" content="Computer-readable conlang data for use to build APIs and other products">
  <meta name="author" content="Rachel Singh">

  <link rel="stylesheet" href="basic.css">

</head>

<body>
    <h1> Archive @ ayadanconlangs.com </h1>
    
    <p>
        <a href="http://api.ayadanconlangs.com">API</a> | 
        <a href="http://archive.ayadanconlangs.com">Archive</a> | 
        <a href="http://data.ayadanconlangs.com">Data</a> | 
        <a href="http://tools.ayadanconlangs.com">Tools</a> | 
        <a href="http://www.ayadanconlangs.com">Main website</a>
    </p>
    
    <p>
        This page provides documentation about the APIs provided
        through ayadanconlangs.com.
    </p>
    
    <p><strong>Repository:</strong> <a href="https://bitbucket.org/ayadan/archive.ayadanconlangs.com">https://bitbucket.org/ayadan/archive.ayadanconlangs.com</a></p>
    
    <hr>
    
    <h2>Directory files</h2>
    
    <?
    function PrintDirectory( $path )
    {
        $files = scandir( $path );
        
        foreach( $files as $key => $value )
        {
            // Ignore these
            if ( $value == "." || $value == ".." || $value == ".git" || $value == "index.php" )
            {
                continue;
            }
            
            // Folder: Recurse into
            else if ( is_dir( $path . "/" . $value ) )
            {
                echo( "<li class='foldable collapsed'>$value/<ul>" );
                PrintDirectory( $path . "/" . $value, $directoryInfo );
                echo( "</ul></li>" );
            }
            
            // File: Display link
            else
            {                
                echo( "<li><a href='$path/$value'> $value </a></li>" );
            }
        }
    }
    
    echo( "<ul>" );
    PrintDirectory( "." );
    echo( "</ul>" );
    ?>
    
    <script
      src="https://code.jquery.com/jquery-3.4.1.min.js"
      integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
      crossorigin="anonymous"></script>
      
      <script>
          $( document ).ready( function() {
              $( ".foldable" ).click( function() {
                  if ( $( this ).hasClass( "collapsed" ) )
                  {
                      $( this ).addClass( "expanded" );
                      $( this ).removeClass( "collapsed" );
                  }
                  else
                  {
                      $( this ).addClass( "collapsed" );
                      $( this ).removeClass( "expanded" );
                  }
              } );
          } );
      </script>
</body>
</html>
